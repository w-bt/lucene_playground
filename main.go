package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/urfave/cli"
	"io/ioutil"
	"log"
	"math"
	"os"
	"regexp"
	"sort"
	"strings"
)

type doc struct {
	fileName   string
	score float64
}

type docList []doc

func (e docList) Len() int {
	return len(e)
}

func (e docList) Less(i, j int) bool {
	return e[i].score > e[j].score
}

func (e docList) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}

var (
	reg *regexp.Regexp
	generateDictionaryCmd = cli.Command{
		Name:  "generate_dictionary",
		Usage: "create dictionary",
		Action: func(c *cli.Context) error {
			fileName := c.Args().Get(0)
			return generateDictionary(fileName)
		},
	}

	calculateTFCmd = cli.Command{
		Name:  "calculate_tf",
		Usage: "calculate tf",
		Action: func(c *cli.Context) error {
			query := c.Args().Get(0)
			return calculateTF(query)
		},
	}

	calculateIDFCmd = cli.Command{
		Name:  "calculate_idf",
		Usage: "calculate idf",
		Action: func(c *cli.Context) error {
			query := c.Args().Get(0)
			return calculateIDF(query)
		},
	}

	calculateTFIDFCmd = cli.Command{
		Name:  "calculate_tfidf",
		Usage: "calculate tfidf",
		Action: func(c *cli.Context) error {
			query := c.Args().Get(0)
			return calculateTFIDF(query)
		},
	}

	calculateBM25Cmd = cli.Command{
		Name:  "calculate_bm25",
		Usage: "calculate bm25",
		Action: func(c *cli.Context) error {
			query := c.Args().Get(0)
			return calculateBM25(query)
		},
	}
)

func init(){
	var err error
	reg, err = regexp.Compile("[^a-zA-Z0-9]+")
	if err != nil {
		log.Fatal(err)
	}
}

func main(){
	clientApp := cli.NewApp()
	clientApp.Name = "lucene_playground"
	clientApp.Version = "1.0.0"
	clientApp.Commands = []cli.Command{
		generateDictionaryCmd,
		calculateTFCmd,
		calculateIDFCmd,
		calculateTFIDFCmd,
		calculateBM25Cmd,
	}

	if err := clientApp.Run(os.Args); err != nil {
		panic(err)
	}
}

func generateDictionary(fileName string) error {
	f, err := os.Open(fmt.Sprintf("corpus/%s", fileName))
	if err != nil {
		return err
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	counter := make(map[string]int)
	var bagOfWords []string

	for scanner.Scan() {
		lowerCase := strings.ToLower(scanner.Text())
		words := strings.Fields(lowerCase)
		bagOfWords = append(bagOfWords, words...)
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	for _, word := range bagOfWords {
		// removing non alphanumeric from string
		finalWord := reg.ReplaceAllString(word, "")
		if finalWord == "" {
			continue
		}
		counter[finalWord] = counter[finalWord] + 1
	}

	jsonByte, err := json.Marshal(counter)
	if err != nil {
		return err
	}

	saveDictionary(jsonByte, fileName)

	log.Printf("Done")

	return nil
}

func saveDictionary(jsonByte []byte, fileName string) {
	finalFileName := strings.Replace(fileName, ".txt", ".json", -1)
	f, err := os.Create(fmt.Sprintf("dictionary/%s", finalFileName))
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	_, err = f.WriteString(string(jsonByte))
	if err != nil {
		log.Fatal(err)
	}
}

func calculateTF(query string) error {
	dictionaryPath := "dictionary"
	files, err := ioutil.ReadDir(dictionaryPath)
	if err != nil {
		return err
	}

	for _, file := range files {
		content, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", dictionaryPath, file.Name()))
		if err != nil {
			return err
		}

		dictionary := make(map[string]int)
		err = json.Unmarshal(content, &dictionary)
		if err != nil {
			return err
		}

		value, exist := dictionary[query]
		if !exist {
			log.Printf("Word %s is not found in %s", query, file.Name())
			continue
		}

		log.Printf("======================\n")
		// Raw TF
		log.Printf("[%s] Term Frequency (RAW) for word %s is %d", file.Name(), query, value)
		// Boolean TF
		boolTF := 0
		if value > 0 {
			boolTF = 1
		}
		log.Printf("[%s] Term Frequency (Boolean) for word %s is %v", file.Name(), query, boolTF)
		// TF Adjusted For Document Length
		total := getTotalWords(dictionary)
		log.Printf("[%s] Term Frequency (TF Adjusted For Document Length) for word %s is (%d/%.0f=%v)", file.Name(), query, value, total, float64(value)/total)
		// Logarithmic TF (from raw TF)
		log.Printf("[%s] Term Frequency (Logarithmic Raw) for word %s is %v", file.Name(), query, math.Log(float64(1 + value)))
		// Augmented TF
		maxFreq := getMaxFrequency(dictionary)
		augmentedTF := 0.5 + 0.5 + float64(value)/maxFreq
		log.Printf("[%s] Term Frequency (Augmented) for word %s is %v", file.Name(), query, augmentedTF)
		// Sqrt TF (Lucine)
		log.Printf("[%s] Term Frequency (Lucine) for word %s is (sqrt(%d)=%v)", file.Name(), query, value, math.Sqrt(float64(value)))
	}

	return nil
}

func getTotalWords(dictionary map[string]int) (total float64) {
	for _, counter := range dictionary {
		total += float64(counter)
	}

	return
}

func getMaxFrequency(dictionary map[string]int) (total float64) {
	for _, counter := range dictionary {
		if float64(counter) > total {
			total = float64(counter)
		}
	}

	return
}

func calculateIDF(query string) error {
	dictionaryPath := "dictionary"
	files, err := ioutil.ReadDir(dictionaryPath)
	if err != nil {
		return err
	}

	occurredDoc := 0
	dictionaries := make(map[string]map[string]int)
	for _, file := range files {
		content, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", dictionaryPath, file.Name()))
		if err != nil {
			return err
		}

		dictionary := make(map[string]int)
		err = json.Unmarshal(content, &dictionary)
		if err != nil {
			return err
		}

		dictionaries[file.Name()] = dictionary

		_, exist := dictionary[query]
		if exist {
			occurredDoc += 1
		}
	}

	log.Printf("Inverse Document Frequency for word %s is (log(%d/%d)=%v)", query, len(files), occurredDoc, math.Log(float64(len(files))/float64(occurredDoc)))
	log.Printf("Inverse Document Frequency (smooth) for word %s is (1 + (log(%d / %d + 1) = %v))", query, len(files), occurredDoc, 1 + (math.Log(float64(len(files))/(float64(occurredDoc)+1))))
	maxOccurrence := getMaxOccurrence(dictionaries)
	log.Printf("Inverse Document Frequency (max) for word %s is (log(%v / %d + 1) = %v)", query, maxOccurrence, occurredDoc, math.Log(maxOccurrence/(float64(occurredDoc)+1)))
	log.Printf("Inverse Document Frequency (probalistic) for word %s is (log(%d-%d / %d) = %v)", query, len(files), occurredDoc, occurredDoc, math.Log(float64(len(files)-occurredDoc)/float64(occurredDoc)))

	return nil
}

func getMaxOccurrence(dictionaries map[string]map[string]int) (total float64) {
	for _, dictionary := range dictionaries {
		for _, counter := range dictionary {
			if float64(counter) > total {
				total = float64(counter)
			}
		}
	}

	return total
}

func calculateTFIDF(query string) error {
	dictionaryPath := "dictionary"
	files, err := ioutil.ReadDir(dictionaryPath)
	if err != nil {
		return err
	}

	occurredDocMap := make(map[string]int)
	dictionaries := make(map[string]map[string]int)
	terms := strings.Fields(query)
	for _, file := range files {
		content, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", dictionaryPath, file.Name()))
		if err != nil {
			return err
		}

		dictionary := make(map[string]int)
		err = json.Unmarshal(content, &dictionary)
		if err != nil {
			return err
		}
		dictionaries[file.Name()] = dictionary

		// count term occurency across documents
		for _, term := range terms {
			_, exist := dictionary[term]
			if exist {
				occurredDocMap[term] += 1
			}
		}
	}

	var totalRawTFIDFList, totalAdjustedTFIDFList, totalLogTFIDFList, totalAugmentedTFIDFList, totalClassicTFIDFList []doc
	for fileName, dictionary := range dictionaries {
		var totalRawTFIDF, totalAdjustedTFIDF, totalLogTFIDF, totalAugmentedTFIDF, totalClassicTFIDF float64

		for _, term := range terms {
			idf := math.Log(float64(len(files)) / float64(occurredDocMap[term]))
			normalizedIDF := 1 + (math.Log(float64(len(files)) / (float64(occurredDocMap[term]) + 1)))
			boost := float64(1)

			value, exist := dictionary[term]
			if !exist {
				continue
			}

			// ||||||||||||||||||||||||||||||||||||||||||
			// Raw TF
			rawTFIDF := float64(value)*idf
			totalRawTFIDF += rawTFIDF
			// ||||||||||||||||||||||||||||||||||||||||||
			// TF Adjusted For Document Length
			total := getTotalWords(dictionary)
			adjustedTFIDF :=(float64(value)/total)*idf
			totalAdjustedTFIDF += adjustedTFIDF
			// ||||||||||||||||||||||||||||||||||||||||||
			// Logarithmic TF (from raw TF)
			logTFIDF := math.Log(float64(1 + value))*idf
			totalLogTFIDF += logTFIDF
			// ||||||||||||||||||||||||||||||||||||||||||
			// Augmented TF
			maxFreq := getMaxFrequency(dictionary)
			augmentedTF := 0.5 + 0.5 + float64(value)/maxFreq
			augmentedTFIDF := augmentedTF*idf
			totalAugmentedTFIDF += augmentedTFIDF
			// ||||||||||||||||||||||||||||||||||||||||||
			// Classic Lucine TF
			// sqrt(tf) * (1 + log(numDocs / (docFreq + 1))) * boost * (1/sqrt(length))
			classicTFIDF := math.Sqrt(float64(value)) * normalizedIDF * boost * 1/(math.Sqrt(total))
			totalClassicTFIDF += classicTFIDF
			// ||||||||||||||||||||||||||||||||||||||||||
		}

		if totalRawTFIDF > 0 {
			totalRawTFIDFList = append(totalRawTFIDFList, doc{
				fileName: fileName,
				score:    totalRawTFIDF,
			})
		}
		if totalAdjustedTFIDF > 0 {
			totalAdjustedTFIDFList = append(totalAdjustedTFIDFList, doc{
				fileName: fileName,
				score:    totalAdjustedTFIDF,
			})
		}
		if totalLogTFIDF > 0 {
			totalLogTFIDFList = append(totalLogTFIDFList, doc{
				fileName: fileName,
				score:    totalLogTFIDF,
			})
		}
		if totalAugmentedTFIDF > 0 {
			totalAugmentedTFIDFList = append(totalAugmentedTFIDFList, doc{
				fileName: fileName,
				score:    totalAugmentedTFIDF,
			})
		}
		if totalClassicTFIDF > 0 {
			totalClassicTFIDFList = append(totalClassicTFIDFList, doc{
				fileName: fileName,
				score:    totalClassicTFIDF,
			})
		}
	}

	// sort TFIDF Raw
	sort.Sort(docList(totalRawTFIDFList))
	log.Printf("Result for TFIDF (Raw)")
	for _, docInfo := range totalRawTFIDFList {
		fmt.Printf("Document: %s Score %v\n", docInfo.fileName, docInfo.score)
	}
	log.Printf("======================")
	// sort TFIDF Adjusted
	sort.Sort(docList(totalAdjustedTFIDFList))
	log.Printf("Result for TFIDF (Adjusted)")
	for _, docInfo := range totalAdjustedTFIDFList {
		fmt.Printf("Document: %s Score %v\n", docInfo.fileName, docInfo.score)
	}
	log.Printf("======================")
	// sort TFIDF Log
	sort.Sort(docList(totalLogTFIDFList))
	log.Printf("Result for TFIDF (Log)")
	for _, docInfo := range totalLogTFIDFList {
		fmt.Printf("Document: %s Score %v\n", docInfo.fileName, docInfo.score)
	}
	log.Printf("======================")
	// sort TFIDF Augmented
	sort.Sort(docList(totalAugmentedTFIDFList))
	log.Printf("Result for TFIDF (Augmented)")
	for _, docInfo := range totalAugmentedTFIDFList {
		fmt.Printf("Document: %s Score %v\n", docInfo.fileName, docInfo.score)
	}
	log.Printf("======================")
	// sort TFIDF Classic
	sort.Sort(docList(totalClassicTFIDFList))
	log.Printf("Result for TFIDF (Classic)")
	for _, docInfo := range totalClassicTFIDFList {
		fmt.Printf("Document: %s Score %v\n", docInfo.fileName, docInfo.score)
	}
	log.Printf("======================")

	return nil
}

func calculateBM25(query string) error {
	dictionaryPath := "dictionary"
	files, err := ioutil.ReadDir(dictionaryPath)
	if err != nil {
		return err
	}

	dictionaries := make(map[string]map[string]int)
	documentLength := make(map[string]float64)
	var totalDocumentLength float64

	occurredDocMap := make(map[string]int)
	terms := strings.Fields(query)
	for _, file := range files {
		content, err := ioutil.ReadFile(fmt.Sprintf("%s/%s", dictionaryPath, file.Name()))
		if err != nil {
			return err
		}

		dictionary := make(map[string]int)
		err = json.Unmarshal(content, &dictionary)
		if err != nil {
			return err
		}
		dictionaries[file.Name()] = dictionary

		for _, term := range terms {
			_, exist := dictionary[term]
			if exist {
				occurredDocMap[term] += 1
			}
		}

		total := getTotalWords(dictionary)
		totalDocumentLength += total
		documentLength[file.Name()] = total
	}

	var totalBM25List []doc
	for fileName, dictionary := range dictionaries {
		var totalScore float64
		for _, term := range terms {
			D := float64(len(files))
			d := float64(occurredDocMap[term])
			adjustedIDF := math.Log(1 + (D-d+0.5)/(d+0.5))
			averageDocumentLength := totalDocumentLength / float64(len(documentLength))
			boost := float64(1)

			value, exist := dictionary[term]
			if !exist {
				continue
			}

			b := 0.75 // between 0 - 1
			B := (1-b) + (b * (documentLength[fileName]/averageDocumentLength))
			tf := math.Sqrt(float64(value))
			k := 1.2
			multiplier := k + 1
			adjustedTF := (multiplier * tf) / ((tf + k) * B)
			luceneBM25 := adjustedIDF * boost * adjustedTF
			totalScore += luceneBM25
		}

		if totalScore > 0 {
			totalBM25List = append(totalBM25List, doc{
				fileName: fileName,
				score:    totalScore,
			})
		}
	}

	// sort BM25
	sort.Sort(docList(totalBM25List))
	log.Printf("Result for BM25")
	for _, docInfo := range totalBM25List {
		fmt.Printf("Document: %s Score %v\n", docInfo.fileName, docInfo.score)
	}
	log.Printf("======================")

	return nil
}